import React from 'react';

const userInput = (props) => {

    const style = {
        border: '1px solid #AAA',
        borderRadius: '4px',
        boxShadow: '1px 1px 3px #8F8F8F',
        padding: '7px 7px',
        width: '200px'
    }

    return (
        <input type='text' style={style} placeholder="username" onChange={props.change} value={props.username} />
    )
}

export default userInput;