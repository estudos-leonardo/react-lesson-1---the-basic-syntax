import React, { Component } from 'react';
import './App.css';
import UserInput from './UserInput/UserInput'
import UserOutput from './UserOutput/UserOutput'

class App extends Component {

    state = {
        username: 'leonardo_isso'
    }

    defaultUsernameHandler = (defaultUsername) => {

    }

    usernameChangeHandler = (e) => {
        this.setState({
            username: e.target.value
        })
    }

    render() {

        return (
            <div className="App">
                <div style={{margin:'25px auto', width:'214px'}}>
                    <UserInput change={this.usernameChangeHandler.bind(this)} username={this.state.username} />
                    <UserOutput username={this.state.username} />
                    <UserOutput username={this.state.username} />
                    <UserOutput username={this.state.username} />
                </div>
            </div>
        );
    }
}

export default App;
