import React from 'react';
import './UserOutput.css';

const userOutput = (props) => {
    return (
        <div className="card">
            <p>The selected username is:</p>
            <p className="username">{props.username}</p>
        </div>
    )
}

export default userOutput;